# Introduction
The purpose of this project is to support team development and deployment using Hugo, Cloud9 and S3. It includes all the solutions to all the problems we found in creating a reproducable and easy to use process that spans an entire team.

You do not need to use Cloud9 to use this project. Just ignore the cloud9 related details and update your properties file accordingly. Note that you will need to install the aws tools and create an IAM with access to S3.

# Team Development Process
1. Each team member can use his or her own AWS account
1. A member team member creates his or her Cloud9 server withing AWS - very simple
1. A member clones the website repository and execute the steps in the below sample README
1. Each member can either work in a fork of the site or on a separate branch
1. Each member commits and shares changes accordingly
1. Site manager aggregates changes and releases the site

# Getting Started
Below are the steps to install hugo and the necessary tools

1. Create an AWS => Cloud9 environment. If you do not have an AWS account, get one.
1. Clone this repository into your Cloud9 => home directory.
    1. cd; git clone https://cboecking@bitbucket.org/cboecking/hugocloud9s3tools.git ; cd hugocloud9s3tools
1. Install hugo
    1. ./cloud9Init.sh

Notes:

1. You many need to source ~/.profile for your system to see hugo added to ~/bin/hugo (or simply log out and log back in).

# Creating Your First Website Repository from Scratch
Below are the steps to create your first hugo website repository.

1. NOTE: make the ~/environment just in case it does not exist
1. mkdir ~/environment
1. cd ~/environment
1. hugo new site NAMEOFYOURSITE
1. cd NAMEOFYOURSITE
1. git init
1. cd themes
1. NOTE: [review to better understand working with themes](https://www.andrewhoog.com/post/git-submodule-for-hugo-themes/)
1. git submodule add https://github/path/to/your/theme.git
1. cd ..
1. NOTE: copy your theme's config to the project root directory
1. cp themes/THEME_NAME/exampleSite/config.toml .
1. NOTE: create a proper hugo .gitignore
1. cp ~/hugocloud9s3tools/.gitignore .
1. NOTE: you may create as many my_hugo.properties files as you wish. Examples: my_hugo_uat1.properties, myhugo_dev.properties
1. cp ~/hugocloud9s3tools/default_hugo.properties my_hugo.properties
1. NOTE: modify the my_hugo.properties to reflect your AWS account and your wishes
1. git add --all
1. git commit -m "initial commit"
1. NOTE: push to your repository of choice (bitbucket or github - follow their instructions)

Here is the typical README for a website repository like the one above:

USERNAME="SOME_USER"  
WEBSITE="SOME_SITE"

1. cd; git clone https://$USERNAME@bitbucket.org/cboecking/hugocloud9s3tools.git ; cd hugocloud9s3tools
2. ./cloud9Init.sh 
3. source ~/.bashrc 
4. cd ~/environment/
5. git clone https://$USERNAME@bitbucket.org/cboecking/$WEBSITE.git
6. cd $WEBSITE/
8. ./siteInit.sh 
9. vim my_hugo.properties
    9. NOTE: edit PROP_CLOUD9_URL and PROP_ENV_NAME
1. ~/hugocloud9s3tools/createAWSs3Bucket.sh 
1. showmeaws 
    1. NOTE: click on the link in your command prompt to view and share the site

Note: [cheatsheet for working with git submodules](https://bhilburn.org/cheat-sheet-for-git-submodules/)

# Pulling an Existing Hugo Website Repository
1. NOTE: make the ~/environment just in case it does not exist
1. mkdir ~/environment
1. cd ~/environment
1. git clone https://github.com/path/to/your/website/project.git
1. cd theme/THEME_NAME/
1. git submodule update --init
1. NOTE: repeat the previous step for every theme you have included as a submodule
1. cp ~/hugocloud9s3tools/default_hugo.properties my_hugo.properties
1. NOTE: modify the my_hugo.properties to reflect your AWS account and your wishes
1. NOTE: update your .gitignore to include details from ~/hugocloud9s3tools/.gitignore if needed

Note: [cheatsheet for working with git submodules](https://bhilburn.org/cheat-sheet-for-git-submodules/)

# Preview Your Hugo Site in Cloud9
1. Find your Cloud9 URL
    1. In Cloud9 IDE => click on Menu => Preview => Preview Running Application => Click in URL bar in Preview pane
    1. Replace the existing URL in ~/environment/YOURPROJECT/env my_hugo.properties
1. From your project's directory, execute: ~/hugocloud9s3tools/hugoServePreview.sh
1. NOTE: you can also use the installed alias of "showmepreview"
1. Open the preview window (In Cloud9 IDE => click on Menu => Preview => Preview Running Application)
1. NOTE: The cloud9 preview does not work well in all regions. 
1. NOTE: Instead, you can simply click the link in the console and view in any browser/tab.

# Create an AWS Bucket to Server Your Site
1. update the ~/environment/YOURPROJECT/env my_hugo.properties to set the desired bucket and aws region
1. Execute ~/hugocloud9s3tools/createAWSs3Bucket.sh
1. NOTE: you only need to perform this task once per bucket
1. NOTE: you may create as many buckets as you need
1. NOTE: You can use different my_hugo*.properties to push to different buckets
1. EXAMPLE: ~/hugocloud9s3tools/createAWSs3Bucket.sh -p my_hugo_prod.properties

# Push Your Site to Your AWS Bucket
1. Push your site to AWS S3 by executing ~/hugocloud9s3tools/hugoDeployToS3.sh
1. NOTE: you can also use the installed alias of "showmeaws"
1. NOTE: You can use different my_hugo_properties to push to different buckets
1. EXAMPLE: showmeaws -p my_hugo_prod.properties

# General Notes
1. The script pulls a specific version of Hugo (not the latest). This is on purpose to ensure consistency between environments and builds.

# Roadmap and Wishlist
1. Update the script to confirm ssl (production only)
1. Update the script to configure cloudfront CDN (production only)
1. Update the site to [prompt for username password](https://hackernoon.com/serverless-password-protecting-a-static-website-in-an-aws-s3-bucket-bfaaa01b8666)

# Acknowledgements
1. https://github.com/Zenoguy3 for creating the first version of this script
