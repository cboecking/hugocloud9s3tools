#!/bin/bash

# check for default_hugo.properties
if [ -f default_hugo.properties ]
then
    source default_hugo.properties
fi

# check for my_hugo.properties
if [ -f my_hugo.properties ]
then
    source my_hugo.properties
fi

# check for command line properties
# Special thanks to https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/
# variables will be processed in the order they appear on the command line
# the colon after the letter specifies there should be text with the option

# Step #1 - set the variables in OPTSTRING
OPTSTRING="hp:"

while getopts $OPTSTRING option; do
    case "${option}" in

        # Step #2 - handle variables
        h) echo "Usage:"
            echo "-p    Specify alternate properties [other than my_hugo.properties]"
            echo "-h    Help"
            exit 0
            ;;

        p) source ${OPTARG};;
    esac
done

# check if variable are populated (some property file passed in)
if [ -z "$PROP_HUGO_VERSION" ]
then
      echo "Exiting! No properties passed in."
      exit 1
fi

./installHugo.sh

# Make ~/environment if not already exists
mkdir -p ~/environment

# Add aliases
echo alias showmepreview="~/hugocloud9s3tools/hugoServePreview.sh" >> ~/.bashrc
echo alias showmeaws="~/hugocloud9s3tools/hugoDeployToS3.sh" >> ~/.bashrc
echo alias gitcommit="~/hugocloud9s3tools/gitCommit.sh" >> ~/.bashrc
echo
echo created the following shortcuts:
echo showmepreview - used to launch preview in cloud9
echo showmeaws - used to push the current site to AWS
echo issue the following command to use them: source ~/.bashrc
