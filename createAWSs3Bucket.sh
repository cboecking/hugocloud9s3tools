#!/bin/bash

# check for default_hugo.properties
if [ -f default_hugo.properties ]
then
    source default_hugo.properties
fi

# check for my_hugo.properties
if [ -f my_hugo.properties ]
then
    source my_hugo.properties
fi

# check for command line properties
# Special thanks to https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/
# variables will be processed in the order they appear on the command line
# the colon after the letter specifies there should be text with the option

# Step #1 - set the variables in OPTSTRING
OPTSTRING="hp:"

while getopts $OPTSTRING option; do
    case "${option}" in

        # Step #2 - handle variables
        h) echo "Usage:"
            echo "-p    Specify alternate properties [other than my_hugo.properties]"
            echo "-h    Help"
            exit 0
            ;;

        p) source ${OPTARG};;
    esac
done

# check if variables are populated (some property file passed in)
if [ -z "$PROP_HUGO_VERSION" ]
then
      echo "Exiting! No properties passed in."
      exit 1
fi
echo "${PROP_ENV_NAME}"
BUCKET_NAME="${PROP_ENV_NAME}"
LOG_BUCKET_NAME="${BUCKET_NAME}-logs"

# One fresh bucket please!
aws s3 mb s3://$BUCKET_NAME --region $PROP_AWS_REGION
# And another for the logs
aws s3 mb s3://$LOG_BUCKET_NAME --region $PROP_AWS_REGION

# Let AWS write the logs to this location
aws s3api put-bucket-acl --bucket $LOG_BUCKET_NAME \
--grant-write 'URI="http://acs.amazonaws.com/groups/s3/LogDelivery"' \
--grant-read-acp 'URI="http://acs.amazonaws.com/groups/s3/LogDelivery"'

# Setup logging
LOG_POLICY="{\"LoggingEnabled\":{\"TargetBucket\":\"$LOG_BUCKET_NAME\",\"TargetPrefix\":\"$BUCKET_NAME\"}}"
aws s3api put-bucket-logging --bucket $BUCKET_NAME --bucket-logging-status $LOG_POLICY

# Create website config
echo "{
    \"IndexDocument\": {
        \"Suffix\": \"index.html\"
    },
    \"ErrorDocument\": {
        \"Key\": \"404.html\"
    },
    \"RoutingRules\": [
        {
            \"Redirect\": {
                \"ReplaceKeyWith\": \"index.html\"
            },
            \"Condition\": {
                \"KeyPrefixEquals\": \"/\"
            }
        }
    ]
}" > website.json

aws s3api put-bucket-website --bucket $BUCKET_NAME --website-configuration file://website.json

rm website.json
