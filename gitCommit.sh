#!/bin/bash

# Used as a shortcut for writers who do not know git.
# This script assumes that each write either has their own branch or own fork to avoid conflicts.


# check for command line properties
# Special thanks to https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/
# variables will be processed in the order they appear on the command line
# the colon after the letter specifies there should be text with the option

# Step #1 - set the variables in OPTSTRING
OPTSTRING="hm:"

while getopts $OPTSTRING option; do
    case "${option}" in

        # Step #2 - handle variables
        h) echo "Usage:"
            echo "-h    Help"
            echo "-m    Message that will be used to describe the commit"
            exit 0
            ;;

        m) TEMP_GIT_MESSAGE=${OPTARG};;
    esac
done

# check if variables are populated
if [ -z "$TEMP_GIT_MESSAGE" ]
then
      echo "Exiting! No message passed in. Example: '-m somemessage in quotes'"
      exit 1
fi

git add --all
git commit -m $TEMP_GIT_MESSAGE
git push
