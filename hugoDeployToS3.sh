#!/bin/bash

# check for default_hugo.properties
if [ -f default_hugo.properties ]
then
    source default_hugo.properties
fi

# check for my_hugo.properties
if [ -f my_hugo.properties ]
then
    source my_hugo.properties
fi

# check for command line properties
# Special thanks to https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/
# variables will be processed in the order they appear on the command line
# the colon after the letter specifies there should be text with the option

# Step #1 - set the variables in OPTSTRING
OPTSTRING="p:a:b:h"

while getopts $OPTSTRING option; do
    case "${option}" in

        # Step #2 - handle variables
        a) HUGOARGS=${OPTARG};;

        b) PROP_ENV_NAME=${OPTARG};;

        h) echo "Usage:"
            echo "-a    Hugo commend line arguments"
            echo "-b    AWS Bucket name"
            echo "-p    Specify alternate properties [other than my_hugo.properties]"
            echo "-h    Help"
            exit 0
            ;;

        p) source ${OPTARG};;
    esac
done

# check if variables are populated (some property file passed in)
if [ -z "$PROP_HUGO_VERSION" ]
then
      echo "Exiting! No properties passed in."
      exit 1
fi

# temporarily replace baseURL with current
# TODO: need to change to https when createBucket is updated accordingly
TEMP_BASE_URL="http://$PROP_ENV_NAME.s3-website.$PROP_AWS_REGION.amazonaws.com/"
sed -i '/^baseURL/ c\baseURL = "'$TEMP_BASE_URL'"' config.toml

hugo $HUGOARGS

# if your site has lots of images in static/ and you do not want them duplicated as part of the upload, add the following:
# --exclude 'static/*'
aws s3 sync --acl "public-read" public/ s3://$PROP_ENV_NAME --exclude 'post'

# restore baseURL
sed -i '/^base[Uu][Rr][Ll]/ c\baseURL = "https://www.example.com"' config.toml

echo "you can see changes at $TEMP_BASE_URL"
