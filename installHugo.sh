#!/bin/bash

# check for default_hugo.properties
if [ -f default_hugo.properties ]
then
    source default_hugo.properties
fi

# check for my_hugo.properties
if [ -f my_hugo.properties ]
then
    source my_hugo.properties
fi

# check for command line properties
# Special thanks to https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/
# variables will be processed in the order they appear on the command line
# the colon after the letter specifies there should be text with the option

# Step #1 - set the variables in OPTSTRING
OPTSTRING="lhp:"

while getopts $OPTSTRING option; do
    case "${option}" in

        # Step #2 - handle variables
        h) echo "Usage:"
            echo "-h    Help"
            echo "-l    Install latest version of hugo"
            echo "-p    Specify alternate properties [other than my_hugo.properties]"
            exit 0
            ;;

        l) TEMP_HUGO_LATEST="Y";;
        p) source ${OPTARG};;
    esac
done

# check if variables are populated (some property file passed in)
if [ -z "$PROP_HUGO_VERSION" ]
then
      echo "Exiting! No properties passed in."
      exit 1
fi
# Create the ~/bin directory (if it does not exist):
mkdir -p ~/bin

# Delete hugo if already exists - allows to reinstall as needed
rm ~/bin/hugo

# Download the project's baseline Hugo release - test if variable is empty
if [ -z "$TEMP_HUGO_LATEST" ]
then
    echo "Installing project baseline version of hugo: $PROP_HUGO_VERSION"
    wget https://github.com/gohugoio/hugo/releases/download/v"$PROP_HUGO_VERSION"/hugo_extended_"$PROP_HUGO_VERSION"_Linux-64bit.tar.gz -q -O - | tar xvz -C ~/bin hugo
else
    # Special thanks to https://gist.github.com/steinwaywhw/a4cd19cda655b8249d908261a62687f8#gistcomment-2172418
    echo "Installing latest version of hugo. Execute to find the version: hugo verion"
    curl -s https://api.github.com/repos/gohugoio/hugo/releases/latest | grep browser_download_url | grep Linux-64bit.tar.gz | cut -d '"' -f 4 | wget -q -i - -O - | tar xvz -C ~/bin hugo
fi
