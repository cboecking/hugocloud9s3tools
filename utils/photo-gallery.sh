# Things to improve
#  1. allow for adding additional images after the first publication - new images at top - is there an option for a separator??

# This purpose of this file is to give you an idea of how to create photo gallery from hugo.
# This file is not really a script; however, it is simply a collection of commands you can copy, modify and paste as is needed.

# This assumes script you have access to an AWS account...

# Tool: Hugo - static site generator
# Theme: Hugrid
# follow directions to copy the example site config.toml and /data directories from theme github site

# where do you keep your stuff
CODE_DIR=~/code/website-family-photos/
cd $CODE_DIR/

# create a new site
SITE_NAME=2019-soccer-george
hugo new site $SITE_NAME
cd $SITE_NAME/

# download the theme
cd themes/
git clone https://github.com/aerohub/hugrid
cd $CODE_DIR/$SITE_NAME/

# copy example config.toml
cp themes/hugrid/exampleSite/config.toml .

# make your images directory
IMG_DIR=static/images/
mkdir -p $IMG_DIR
cd $IMG_DIR

# copy the images you care about here...
# cp ...
# NOTE: this script/file assumes all images in case sensitive *.JPG

# make thumbnails of existing images
# install convert: sudo apt install imagemagick
for i in `ls *.JPG`; do convert -thumbnail 380 $i thumb.$i; done

# go back to main site folder
cd $CODE_DIR/$SITE_NAME/

# update data/items.toml according to all images
# assumes cannon camera and therefore names of images (not thumbnails) begin with IMG
# note:'images' reference is hard coded here - this needs to be made more configurable
for i in `(cd $IMG_DIR && ls IMG*.JPG)`; do echo -e "[[items]]\ntitle = \"$i\"\nimage = \"images/$i\"\nthumb = \"images/thumb.$i\"" >> data/items.toml; done

# preview the site using:
hugo server -D

# update my_hugo.properties according to main project details
# create an aws bucket using ~/hugocloud9s3tools/createAWSs3Bucket.sh
# push to aws using showmeaws command